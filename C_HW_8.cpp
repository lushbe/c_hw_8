#include <iostream>
#include <string>

using namespace std;


class Animal
{
private:

    string Phrase = "Hello";

public:

    virtual string Voice() 
    {

        return Phrase;
    }


};

class Dog : public Animal
{
private:

    string Phrase = "Woof";

public:

     string Voice() override
    {

        return Phrase;
    }


};

class Fox : public Animal
{
private:

    string Phrase = "What the fox say?";

public:

    string Voice() override
    {

        return Phrase;
    }


};

class Cat : public Animal
{
private:

    string Phrase = "Meow";

public:

    string Voice() override
    {

        return Phrase;
    }


};

class Goverment : public Animal
{
private:

    string Phrase = "Need more monetary fine";

public:

    string Voice() override
    {

        return Phrase;
    }


};

int main()
{


    Animal* p[] = { new Dog, new Fox, new Cat, new Goverment };
    for (auto i : p)
    {
       std::cout << i->Voice() << '\n';
    }
}

